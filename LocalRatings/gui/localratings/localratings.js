function init(data)
{

    let localRatingsPage = new LocalRatingsPage();
    let ratingsDB = new LocalRatingsRatingsDB();
    let replayDB = new LocalRatingsReplayDB();

    // Check if replay database is populated. If not, run the first run dialog.
    replayDB.load();
    if (replayDB.empty())
    {
        Engine.PushGuiPage("page_localratings_firstrundialog.xml", {}, data => {
            if (data.update)
            {
                localRatingsPage.onRebuildButtonPress();
                ratingsDB.load();
                if (ratingsDB.empty())
                    Engine.PushGuiPage("page_localratings_emptylistdialog.xml");
            }
        });
        return;
    }

    // Check if database version is the current one. If not, run the update required dialog.
    const cache = new LocalRatingsCache();
    if (cache.isUpdateRequired())
    {
        Engine.PushGuiPage("page_localratings_updaterequireddialog.xml", {}, data => {
            if (data.update)
            {
                localRatingsPage.onRebuildButtonPress();
                ratingsDB.load();
                if (ratingsDB.empty())
                    Engine.PushGuiPage("page_localratings_emptylistdialog.xml");
            }
        });
        return;
    }

    // All tests passed. Populate table
    ratingsDB.load();
    localRatingsPage.ratingsDatabase = ratingsDB.ratingsDatabase;
    localRatingsPage.historyDatabase = ratingsDB.historyDatabase;
    localRatingsPage.updateList();

}
