# About LocalRatings

*LocalRatings* is a [0 A.D.](https://play0ad.com) mod aimed at ranking players, rating their performance and providing statistical data based on previously played games.

![](img/Main1.png "Main page with evolution charts")
![](img/Main2.png "Main page with civilization charts")

# Installation

1. Download the source code of the [latest release](https://gitlab.com/mentula0ad/LocalRatings/-/releases) and extract it.
2. Open the extracted folder and locate the sub-folder named `LocalRatings`.
3. Copy the `LocalRatings` folder into the `mods` folder, tipically located at:
   * Linux: `~/.local/share/0ad/mods/`
   * macOS: `~/Library/Application\ Support/0ad/mods/`
   * Windows: `~\Documents\My Games\0ad\mods\`
4. Launch 0 A.D. and open the `Settings > Mod Selection` menu.
5. Select the LocalRatings mod, `Enable` it and `Save Configuration`.

# The rating system

The rating system used by *LocalRatings* to evaluate players is explained [here](https://gitlab.com/mentula0ad/LocalRatings/-/blob/master/ABOUT.md).

Alternatively, you can open the `Local Ratings` menu page in 0 A.D. and click on `About Local Ratings`.

# Features, F.A.Q. and additional information

A list of features and Frequently Asked Questions can be found [here](https://gitlab.com/mentula0ad/LocalRatings/-/blob/master/ABOUT.md).

Alternatively, you can open the `Local Ratings` menu page in 0 A.D. and click on `About Local Ratings`.

The discussion thread on the official 0 A.D. forum is [here](https://wildfiregames.com/forum/topic/80151-localratings-mod-evaluate-players-skills-based-on-previous-games/#comment-497805).
