/**
 * This class helps replacing the PlayerAssignmentItem.Client class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * The createItem() method is responsible for displaying names, hence we want to modify it so that the rating is displayed next to each player's name.
 */
class PlayerAssignmentItemClient_LocalRatings extends PlayerAssignmentItem.Client {

    constructor()
    {
        super();
    }

    createItem(guid)
    {
        const show = Engine.ConfigDB_GetValue("user", "localratings.general.showmatchsetup");
        // If option is active
        if (show === "true")
        {
            // Add rating only if player is not new to the database
            const realPlayerName = g_PlayerAssignments[guid].name.split(" ")[0];
            if (realPlayerName in g_LocalRatingsDatabase)
            {
                const playerData = g_LocalRatingsDatabase[realPlayerName];
                return {
                    "Handler": this,
                    "Value": guid,
                    "Autocomplete": g_PlayerAssignments[guid].name,
                    "Caption": setStringTags(
                        addRating_LocalRatings(g_PlayerAssignments[guid].name, playerData),
                        g_PlayerAssignments[guid].player == -1 ? this.ObserverTags : this.PlayerTags)
                };
            }
        }
        // Otherwise
        return super.createItem(guid);
    }

}

PlayerAssignmentItem.Client = PlayerAssignmentItemClient_LocalRatings;
