/**
 * This class is responsible for interactions with cache files, like loading or saving.
 * It is able to detect whether the database structure has changed, due to installation of a new version of the mod.
 */
class LocalRatingsCache
{

    constructor()
    {
        const version = Engine.GetEngineInfo().mods.filter(x => x.mod == "public")[0].version;
        const path = "localratings/";
        this.replayDatabaseFile = path + version + "/replayDatabase.json";
        this.ratingsDatabaseFile = path + version + "/ratingsDatabase.json";
        this.historyDatabaseFile = path + version + "/historyDatabase.json";
        this.cacheVersionFile = path + version + "/cacheVersion.json";

        this.createCacheFilesIfNotExist();
    }

    createCacheFilesIfNotExist()
    {
        for (let file of [
            this.replayDatabaseFile,
            this.ratingsDatabaseFile,
            this.historyDatabaseFile
        ])
        {
            if (!Engine.FileExists(file))
                Engine.WriteJSONFile(file, {});
        }
    }

    tagToFilename(tag)
    {
        return (tag == "replayDatabase") ?
            this.replayDatabaseFile :
            (tag == "ratingsDatabase") ?
            this.ratingsDatabaseFile :
            (tag == "historyDatabase") ?
            this.historyDatabaseFile :
            "";
    }

    currentVersionExists()
    {
        return Engine.FileExists(this.cacheVersionFile);
    }

    getCurrentVersion()
    {
        return Engine.ReadJSONFile(this.cacheVersionFile).version;
    }

    getNewestVersion()
    {
        return Engine.ReadJSONFile("gui/localratings/cacheVersion.json").version;
    }

    updateVersion()
    {
        Engine.ProfileStart("LocalRatingsCacheUpdateVersion");
        Engine.WriteJSONFile(this.cacheVersionFile, {"version": this.getNewestVersion()});
	Engine.ProfileStop();
    }

    isUpdateRequired()
    {
        return !this.currentVersionExists() || this.getCurrentVersion() != this.getNewestVersion();
    }

    load(tag)
    {
	Engine.ProfileStart("LocalRatingsLoadCacheFile");
        const file = this.tagToFilename(tag);
	const data = Engine.FileExists(file) && Engine.ReadJSONFile(file);
	Engine.ProfileStop();
        return (data) ? data : {};
    }

    save(tag, json)
    {
	Engine.ProfileStart("LocalRatingsSaveCacheFile");
        const file = this.tagToFilename(tag);
	Engine.WriteJSONFile(file, json);
	Engine.ProfileStop();
    }

}
