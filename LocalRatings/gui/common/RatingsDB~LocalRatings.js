/**
 * This class is responsible for updating the ratings and history databases stored in the cache folder.
 */
class LocalRatingsRatingsDB
{

    constructor()
    {
        this.ratingsDatabase = {};
        this.historyDatabase = {};
    }

    empty()
    {
        return Object.keys(this.ratingsDatabase) == 0 || Object.keys(this.historyDatabase) == 0;
    }

    load()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();
        this.ratingsDatabase = minifier.magnifyRatingsDatabase(cache.load("ratingsDatabase"));
        this.historyDatabase = minifier.magnifyHistoryDatabase(cache.load("historyDatabase"));
    }

    save()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();
        cache.save("ratingsDatabase", minifier.minifyRatingsDatabase(this.ratingsDatabase));
        cache.save("historyDatabase", minifier.minifyHistoryDatabase(this.historyDatabase));
    }

    rebuild()
    {
        // Run calculator
        let calculator = new LocalRatingsCalculator();
        calculator.rebuild();

        // Update globals
        this.ratingsDatabase = calculator.ratingsDatabase;
        this.historyDatabase = calculator.historyDatabase;
    }

    merge(newReplays)
    {
        // Run calculator
        let calculator = new LocalRatingsCalculator();
        calculator.ratingsDatabase = this.ratingsDatabase;
        calculator.historyDatabase = this.historyDatabase;
        calculator.merge(newReplays);

        // Update globals
        this.ratingsDatabase = calculator.ratingsDatabase;
        this.historyDatabase = calculator.historyDatabase;
    }

}
