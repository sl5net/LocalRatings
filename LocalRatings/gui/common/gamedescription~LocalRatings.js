// Wrapper for the "formatPlayerInfo" function
function formatPlayerInfo_Wrapper_LocalRatings(target, args)
{
    // If global ratings database is not defined, return
    // This happens, for example, in the replay menu page, where the rating shouldn't be displayed
    if (typeof g_LocalRatingsDatabase === "undefined")
        return target(...args);

    // If option is not active, return
    const show = Engine.ConfigDB_GetValue("user", "localratings.general.showplayerprofile");
    if (show !== "true")
        return target(...args);

    // Run original function
    let teamDescription = target(...args);

    // Run additional code
    let teams = teamDescription.split("\n\n");
    let playerDescriptions = teams.map(x => x.split("\n"));
    let playerNamesWithRating = playerDescriptions.map(x => x.map(y => y.startsWith("[color=") ? y.substring(y.indexOf("]") + 1).split("[/color]")[0] : undefined));
    let playerNames = playerNamesWithRating.map(x => x.map(y => (y === undefined) ? undefined : y.startsWith(g_BuddySymbol + " ") ? splitRatingFromNick(y.substring(g_BuddySymbol.length+1, y.length)).nick : splitRatingFromNick(y).nick));

    // Retrieve players data
    let ratings = [];
    for (let teamIndex in playerNames)
    {
        let teamRatings = [];
        for (let playerNamesIndex in playerNames[teamIndex])
        {
            let playerName = playerNames[teamIndex][playerNamesIndex];
            if (playerName === undefined || !(playerName in g_LocalRatingsDatabase))
                teamRatings.push(undefined);
            else
            {
                let playerData = g_LocalRatingsDatabase[playerName];
                teamRatings.push(addRating_LocalRatings("", playerData));
            }
        }
        ratings.push(teamRatings);
    }

    // Rejoin strings
    for (let teamIndex in playerDescriptions)
    {
        for (let playerDescriptionsIndex in playerDescriptions[teamIndex])
        {
            let playerNameWithRating = playerNamesWithRating[teamIndex][playerDescriptionsIndex];
            let rating = ratings[teamIndex][playerDescriptionsIndex];
            if (rating !== undefined)
                playerDescriptions[teamIndex][playerDescriptionsIndex] = playerDescriptions[teamIndex][playerDescriptionsIndex].replace(playerNameWithRating, playerNameWithRating + rating);
        }
    }
    teams = teams.map((x, i) => playerDescriptions[i].join("\n"));
    teamDescription = teams.join("\n\n");

    return teamDescription;
};

// Handler for the "formatPlayerInfo" function
const formatPlayerInfo_Handler_LocalRatings = {
    apply: function(target, thisArg, args) {
        return formatPlayerInfo_Wrapper_LocalRatings(target, args);
    }
};

// Proxy the "formatPlayerInfo" function
formatPlayerInfo = new Proxy(formatPlayerInfo, formatPlayerInfo_Handler_LocalRatings);
