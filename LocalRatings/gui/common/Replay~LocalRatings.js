/**
 * This class represents a replay object.
 * It stores replay data taken from replayCache.json and sequences, taken from the metadata.json in the replay folder.
 */
class LocalRatingsReplay
{

    constructor(metadata)
    {
        this.metadata = metadata;

        try
        {
            this.duration = this.getDuration();
            this.directory = this.getDirectory();
            this.date = this.getDate();
            const playerNames = this.getPlayerNames();
            this.players = playerNames.map(x => this.getPlayerNameWithoutRating(x));
            this.civs = this.getCivs();
            this.teamsSize = this.getTeamsSize();
            this.startingResources = this.getStartingResources();
            this.populationCap = this.getPopulationCap();
            this.worldPopulation = this.getWorldPopulation();
            this.hasAiPlayers = playerNames.some(x => this.isPlayerAI(x));
            this.cheatsEnabled = this.getCheatsEnabled();
            this.revealedMap = this.getRevealedMap();
            this.exploredMap = this.getExploredMap();
            this.nomad = this.getNomad();
            this.scores = playerNames.map(x => this.getScores(x));

            // Check if all keys are defined
            this.isValid = this.validate();
        }
        catch(error)
        {
            this.isValid = false;
        }
        finally
        {
            // Delete metadata, so that object is easily exportable
            delete this.metadata;
        }
    }

    validate()
    {
        return Object.values(this).every(x => typeof x !== 'undefined');
    }

    getDuration()
    {
        return this.metadata.duration;
    }

    getDirectory()
    {
        return this.metadata.directory;
    }

    getDate()
    {
        return this.directory.split("_")[0];
    }

    getPlayerNames()
    {
        return this.metadata.attribs.settings.PlayerData.map(x => x.Name);
    }

    getPlayerNameWithoutRating(playerName)
    {
        return playerName.split(" ")[0];
        return this.metadata.attribs.settings.PlayerData.map(x => x.Name.split(" ")[0]);
    }

    getCivs()
    {
        return this.metadata.attribs.settings.PlayerData.map(x => x.Civ);
    }

    getTeamsSize()
    {
        let teamData = {};
        this.metadata.attribs.settings.PlayerData.forEach(x => (x.Team in teamData) ? teamData[x.Team] += 1 : teamData[x.Team] = 1);
        // Fix the "None" team
        if ("-1" in teamData)
            for (let i=-teamData["-1"]; i<0; i++)
                teamData[i] = 1;
        return teamData;
    }

    getStartingResources()
    {
        return this.metadata.attribs.settings.StartingResources;
    }

    getPopulationCap()
    {
        if (typeof this.metadata.attribs.settings.WorldPopulation !== "undefined")
            return Math.floor(this.metadata.attribs.settings.WorldPopulationCap / this.players.length);
        return this.metadata.attribs.settings.PopulationCap;
    }

    getWorldPopulation()
    {
        return (typeof this.metadata.attribs.settings.WorldPopulation !== "undefined");
    }

    isPlayerAI(playerName)
    {
        return this.metadata.attribs.settings.PlayerData.filter(x => x.Name == playerName)[0].AI;
    }

    getCheatsEnabled()
    {
        return this.metadata.attribs.settings.CheatsEnabled;
    }

    getRevealedMap()
    {
        return this.metadata.attribs.settings.RevealMap;
    }

    getExploredMap()
    {
        return this.metadata.attribs.settings.ExploreMap;
    }

    getNomad()
    {
        return this.metadata.attribs.settings.Nomad || false;
    }

    getScores(playerName)
    {
        const fullMetadata = Engine.GetReplayMetadata(this.directory);
        const sequences = fullMetadata.playerStates.filter(x => x.name == playerName)[0].sequences;
        let sequencesObj = new LocalRatingsSequences(sequences);
        Object.keys(sequencesObj).forEach(x => sequencesObj[x] = getMean_LocalRatings(sequencesObj[x]));
        return sequencesObj;
    }

}
