class LocalRatingsCivilizationChart extends LocalRatingsChart
{

    constructor(player, playerData)
    {
        super(player, playerData);

        // GUI objects
        this.chartContainer = Engine.GetGUIObjectByName("civilizationChartContainer");
        this.currentRatingLine = Engine.GetGUIObjectByName("civilizationCurrentRatingLine");

        // Events
        this.chartContainer.onTick = this.onChartContainerTick.bind(this);

        this.civCharts;
        this.civEmblems;
        this.civEmblemLabels;
        this.civRatings;

        this.series;
        this.chartHeight = 40; // Max height of civ charts, expressed as percent of container height
        this.emblemSize = 60; // The size of the civilization emblems
        this.histogramWidth = 2 // The width of the histogram bars
    }

    getCivSingleGamesRatings(civ)
    {
        return Object.keys(this.playerData).filter(x => this.playerData[x].civ == civ).map(x => this.playerData[x].rating);
    }

    getCivRating(civ)
    {
        const singleGamesRatings = this.getCivSingleGamesRatings(civ);
        if (singleGamesRatings.length == 0)
            return undefined;
        return getMean_LocalRatings(singleGamesRatings);
    }

    getCivMatches(civ)
    {
        return this.getCivSingleGamesRatings(civ).length;
    }

    getCivAdvantage(civRating, currentRating)
    {
        if (civRating === undefined)
            return undefined;
        return civRating - currentRating;
    }

    getCurrentRating()
    {
        const singleGamesRatings = Object.keys(this.playerData).map(x => this.playerData[x].rating);
        return getMean_LocalRatings(singleGamesRatings);
    }

    animate(step)
    {
        if (this.series === undefined)
            return;

        for (let i in this.civCharts)
        {
            let civChart = this.civCharts[i];
            let civChartSize = civChart.size;
            let civEmblem = this.civEmblems[i];
            let civEmblemSize = civEmblem.size;
            let civEmblemLabel = this.civEmblemLabels[i];
            let civEmblemLabelSize = civEmblemLabel.size;
            let civRating = this.civRatings[i];
            let civRatingSize = civRating.size;
            let value = this.series[i];
            let increment = value*step;
            if (value > 0)
            {
                civChartSize.rbottom = 50;
                civChartSize.rtop = 50 - increment;
                civRatingSize.bottom = -this.emblemSize/2;
                civRatingSize.top = -this.emblemSize/2 - 40;
            }
            else
            {
                civChartSize.rtop = 50;
                civChartSize.rbottom = 50 - increment;
                civRatingSize.top = this.emblemSize/2;
                civRatingSize.bottom = this.emblemSize/2 + 40;
            }
            civChart.size = civChartSize;

            civEmblemSize.rbottom = 50 - increment;
            civEmblemSize.rtop = 50 - increment;
            civEmblem.size = civEmblemSize;

            civEmblemLabelSize.rbottom = 50 - increment;
            civEmblemLabelSize.rtop = 50 - increment;
            civEmblemLabel.size = civEmblemLabelSize;

            civRatingSize.rbottom = 50 - increment;
            civRatingSize.rtop = 50 - increment;
            civRating.size = civRatingSize;
        }
    }

    draw()
    {
        // Get system civ info
        const civData = loadCivData();
        const civs = Object.keys(civData);
        const civNames = civs.map(x => civData[x].Name);
        const emblems = civs.map(x => civData[x].Emblem);

        // GUI objects
        this.civCharts = civs.map((x, i) => Engine.GetGUIObjectByName("civChart[" + i + "]"));
        this.civEmblems = civs.map((x, i) => Engine.GetGUIObjectByName("civEmblem[" + i + "]"));
        this.civEmblemLabels = civs.map((x, i) => Engine.GetGUIObjectByName("civEmblemLabel[" + i + "]"));
        this.civRatings = civs.map((x, i) => Engine.GetGUIObjectByName("civRatingText[" + i + "]"));

        // Retrieve data
        const currentRating = this.getCurrentRating();
        const civRatings = civs.map(x => this.getCivRating(x));
        const civMatches = civs.map(x => this.getCivMatches(x));
        const advantages = civRatings.map(x => this.getCivAdvantage(x, currentRating));
        const maxAbsAdvantage = Math.max(Math.max(...advantages.filter(x => x !== undefined)), -Math.min(...advantages.filter(x => x !== undefined)));
        this.series = advantages.map(x => (maxAbsAdvantage == 0 || x === undefined) ? 0 : x*this.chartHeight/maxAbsAdvantage);

        // Distance between histogram bars
        const distance = 100 / this.civCharts.length;

        // Set fixed data of civ charts
        for (let i in this.civCharts)
        {
            let civChart = this.civCharts[i];
            let civChartSize = civChart.size;
            civChartSize.left = -this.histogramWidth/2;
            civChartSize.right = this.histogramWidth/2;
            civChartSize.rleft = distance/2 + distance*i;
            civChartSize.rright = distance/2 + distance*i;
            civChart.size = civChartSize;
        }

        // Set fixed data of emblems
        for (let i in this.civEmblems)
        {
            let civEmblem = this.civEmblems[i];
            let civEmblemLabel = this.civEmblemLabels[i];
            for (let obj of [civEmblem, civEmblemLabel])
            {
                let size = obj.size;
                size.left = -this.emblemSize/2;
                size.right = this.emblemSize/2;
                size.top = -this.emblemSize/2;
                size.bottom = this.emblemSize/2;
                size.rleft = distance/2 + distance*i;
                size.rright = distance/2 + distance*i;
                obj.size = size;
            }
            civEmblem.sprite = (civMatches[i] > 0) ? "stretched:" + emblems[i] : "stretched:grayscale:" + emblems[i];
            civEmblemLabel.tooltip = civNames[i];
        }

        // Set fixed data of info box
        for (let i in this.civRatings)
        {
            let civRating = this.civRatings[i];
            if (civMatches[i] == 0)
                civRating.hidden = true;
            else
            {
                let civRatingSize = civRating.size;
                civRatingSize.rleft = -distance/2 + distance*i;
                civRatingSize.rright = 3*distance/2 + distance*i;
                civRating.size = civRatingSize;
                Engine.GetGUIObjectByName("civRatingText[" + i + "]").caption = formatRating_LocalRatings(civRatings[i]) + " / " + civMatches[i];
                civRating.hidden = false;
            }
        }

        this.chartContainer.hidden = false;
    }

}
