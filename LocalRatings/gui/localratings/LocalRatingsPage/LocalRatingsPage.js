class LocalRatingsPage
{

    constructor()
    {
        this.isColumnSorted = false;

        // GUI objects
        this.searchPlayerInput = Engine.GetGUIObjectByName("searchPlayerInput");
        let cfgSearchplayerinput = Engine.ConfigDB_GetValue("user", "localratings.save.searchplayerinput");
        this.searchPlayerInput.caption = cfgSearchplayerinput;
        this.isSearchPlayerInput_ceated = false;

        this.infoButton = Engine.GetGUIObjectByName("infoButton");

        this.tabEvolutionChart = Engine.GetGUIObjectByName("tabEvolutionChart");
        this.tabEvolutionChartButton = Engine.GetGUIObjectByName("tabEvolutionChartButton");

        this.tabCivilizationChart = Engine.GetGUIObjectByName("tabCivilizationChart");
        this.tabCivilizationChartButton = Engine.GetGUIObjectByName("tabCivilizationChartButton");

        this.tabDividerLeft = Engine.GetGUIObjectByName("tabDividerLeft");
        this.tabDividerRight = Engine.GetGUIObjectByName("tabDividerRight");
        this.playerList = Engine.GetGUIObjectByName("playerList");



        this.playerList.selected_column_order = 1;




        this.mainMenuButton = Engine.GetGUIObjectByName("mainMenuButton");
        this.optionsButton = Engine.GetGUIObjectByName("optionsButton");
        this.rebuildButton = Engine.GetGUIObjectByName("rebuildButton");

        // Events
        this.searchPlayerInput.onTextEdit = this.onSearchPlayerInputTextEdit.bind(this);

        this.infoButton.onPress = this.onInfoButtonPress.bind(this);
        this.tabEvolutionChartButton.onPress = this.onTabEvolutionChartButtonPress.bind(this);
        this.tabCivilizationChartButton.onPress = this.onTabCivilizationChartButtonPress.bind(this);


        this.playerList.onSelectionColumnChange = this.onSelectionColumnChange.bind(this);
        this.playerList.onSelectionChange = this.onSelectionChange.bind(this);
        this.mainMenuButton.onPress = this.onMainMenuButtonPress.bind(this);
        this.optionsButton.onPress = this.onOptionsButtonPress.bind(this);
        this.rebuildButton.onPress = this.onRebuildButtonPress.bind(this);

        // Initialize databases
        this.ratingsDatabase = {};
        this.historyDatabase = {};
        this.initializeDatabases();

        this.columns = {};
        this.currentChart;
        this.currentPlayerProfile;
        this.tabs = [this.tabEvolutionChart, this.tabCivilizationChart];
        this.selectedTab = this.tabs[0];

        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", "");
        // this.chartNowString = Engine.ConfigDB_GetValue("user", "localratings.save.chart");

        const showEvolutionValue = Engine.ConfigDB_GetValue("user", "localratings.charts.showevolution");
        const showEvolutionValueBoolean = (showEvolutionValue === "true");

        // if(this.chartNowString == "evolution"){
        if(showEvolutionValueBoolean === true){
            this.selectedTab = this.tabEvolutionChart;
        }else {
            // error("civi=" + showEvolutionValueBoolean);
            this.selectedTab = this.tabCivilizationChart;
        }

        this.updateTabs();

        if (this.currentChart)
            this.currentChart.clear();
        this.showChart();


        // this.playerList.selected_column = Engine.ConfigDB_GetValue("user", "localratings.save.selected_column");
        // this.playerList.selected_column_order = ( Engine.ConfigDB_GetValue("user", "localratings.save.selected_column_order")) == "-1" ? -1 : 1 ;
        // if(!this.playerList.selected_column){
        //     error("75: selected_column =" + this.playerList.selected_column);
        //     return;
        // }
    }

    // searchPlayerInput events

    onSearchPlayerInputTextEdit()
    {
        // if(!this.playerList.selected_column){
        //     error("86: selected_column =" + this.playerList.selected_column);
        //     return;
        // }
        this.updateList();
    }

    // infoButton events

    onInfoButtonPress()
    {
        Engine.PushGuiPage("page_localratings_about.xml");
    }

    // tab buttons event

    onTabEvolutionChartButtonPress()
    {
        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", "evolution");
        // this.chartNowString = "evolution";
        // Engine.ConfigDB_WriteValueToFile("user", "localratings.save.chart", "evolution", "config/user.cfg");
        Engine.ConfigDB_CreateValue("user", "localratings.charts.showevolution", "true");
        Engine.ConfigDB_WriteValueToFile("user", "localratings.charts.showevolution", "true", "config/user.cfg");

        this.selectedTab = this.tabEvolutionChart;
        this.updateTabs();
        if (this.currentChart)
            this.currentChart.clear();
        this.showChart();
    }

    onTabCivilizationChartButtonPress()
    {
        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", "civilization");
        // this.chartNowString = "civilization";
        // Engine.ConfigDB_WriteValueToFile("user", "localratings.save.chart", "civilization", "config/user.cfg");
        Engine.ConfigDB_CreateValue("user", "localratings.charts.showevolution", "false");
        Engine.ConfigDB_WriteValueToFile("user", "localratings.charts.showevolution", "false", "config/user.cfg");
        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", "evolution");
        
        this.selectedTab = this.tabCivilizationChart;
        this.updateTabs();
        if (this.currentChart)
            this.currentChart.clear();
        this.showChart();
    }

    // playerList events

    onSelectionColumnChange()
    {
        this.updateList();
    }

    onSelectionChange()
    {
        // if (this.currentChart)              // not needet and disturbing if first player in list not change while typing 
        //     this.currentChart.clear();

        if (this.currentPlayerProfile)
            this.currentPlayerProfile.clear();

        this.showChart();
        this.showPlayerProfile();
    }

    // Button events

    onMainMenuButtonPress()
    {        
        if(this.playerList.selected != -1){
            const player = this.playerList.list_playername[this.playerList.selected];
            // Engine.ConfigDB_WriteValueToFile("user", "localratings.save.searchplayerinput", this.searchPlayerInput.caption, "config/user.cfg");
            Engine.ConfigDB_WriteValueToFile("user", "localratings.save.searchplayerinput", player, "config/user.cfg");
        }
        // Engine.ConfigDB_WriteValueToFile("user", "localratings.save.chart", this.chartNowString, "config/user.cfg");
        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", "civilization");
        // Engine.ConfigDB_CreateValue("user", "localratings.save.chart", this.chartNowString);

        Engine.SwitchGuiPage("page_pregame.xml");
    }

    onOptionsButtonPress()
    {
        Engine.PushGuiPage("page_localratings_options.xml", {}, (data) => {
            // If the two databases are defined, then options have been already saved
            if (data.ratingsDatabase && data.historyDatabase)
            {
                // Save globals
                this.ratingsDatabase = data.ratingsDatabase;
                this.historyDatabase = data.historyDatabase;

                // Save ratings and history databases
                let ratingsDB = new LocalRatingsRatingsDB();
                ratingsDB.ratingsDatabase = this.ratingsDatabase;
                ratingsDB.historyDatabase = this.historyDatabase;
                ratingsDB.save();
            }

            // Update list if needed
            if (Object.keys(data.changes).some(x => x.startsWith("localratings.filter.") || x.startsWith("localratings.weight.") || x.startsWith("localratings.playerfilter.")))
                this.updateList();

            // If charts options have changed, update charts
            if (Object.keys(data.changes).some(x => x.startsWith("localratings.charts.")) && this.selectedTab.name == "tabEvolutionChart")
                this.showChart();
        });
    }

    onRebuildButtonPress()
    {
        // Update the replay database with new replays
        let replayDB = new LocalRatingsReplayDB();
        replayDB.rebuild();

        // Update ratings and history databases
        let ratingsDB = new LocalRatingsRatingsDB();
        ratingsDB.rebuild();
        ratingsDB.save();

        // Update globals
        this.ratingsDatabase = ratingsDB.ratingsDatabase;
        this.historyDatabase = ratingsDB.historyDatabase;

        // Push to GUI
        this.updateList();
    }

    // Non-event functions

    initializeDatabases()
    {
        let ratingsDB = new LocalRatingsRatingsDB();
        ratingsDB.load();
        this.ratingsDatabase = ratingsDB.ratingsDatabase;
        this.historyDatabase = ratingsDB.historyDatabase;
    }

    updateTabs()
    {
        let tab = this.selectedTab;
        this.tabs.forEach(x => x.sprite = "ModernTabHorizontalBackground");
        tab.sprite = "ModernTabHorizontalForeground";

        let tabDividerLeftSize = this.tabDividerLeft.size;
        tabDividerLeftSize.rright = tab.size.rleft;
        this.tabDividerLeft.size = tabDividerLeftSize;

        let tabDividerRightSize = this.tabDividerRight.size;
        tabDividerRightSize.rleft = tab.size.rright;
        this.tabDividerRight.size = tabDividerRightSize;
    }

    showChart_ofThisPlayer(player, argumentIsObj)
    { 
        if(this.showChart_show_this_player_at_the_moment === player
            && this.showChart_selectedTab_at_the_moment == this.selectedTab )
            return // dont need for redraw it new

// error('226: showChart_ofThisPlayer: player: ' + player);

        // let player = {};
        // player = playerStr;

        let playerData = null;
        // error('231 argumentIsObj=' + argumentIsObj);

        try {
            // error('234 argumentIsObj=' + argumentIsObj);

            playerData = this.historyDatabase[player];
        } catch (error2) {
            
            let playerNameAsObj = {}; // this ugly trick seems needed for unknown reasons: https://stackoverflow.com/questions/74143407/javascript-convert-undefined-to-object-but-undefined-seems-a-string
            playerNameAsObj = player;
            playerData = this.historyDatabase[playerNameAsObj];
        }
        // const playerData = this.historyDatabase[player];
        // error('this.selectedTab.name= ' + this.selectedTab.name);
        // error('240: this.selectedTab.name: ' + this.selectedTab.name + ", player=" + player);
        if(this.selectedTab.name == ""){
            error('no tab. selectedTab.name: ' + selectedTab.name);
            return false;
        }
        // error('245: this.selectedTab.name: ' + this.selectedTab.name + ", player=" + player);
        if (this.selectedTab.name == "tabEvolutionChart")
            this.currentChart = new LocalRatingsEvolutionChart(player, playerData);
        else if (this.selectedTab.name == "tabCivilizationChart")
            this.currentChart = new LocalRatingsCivilizationChart(player, playerData);

        try {
            this.currentChart.draw();
            Engine.ConfigDB_CreateValue("user", "localratings.save.searchplayerinput", player);
        } catch (error2) {
            // error('near line 289'); // happens to me when config is empty. but not a real problem
        }
        this.showChart_show_this_player_at_the_moment = player;
        this.showChart_selectedTab_at_the_moment = this.selectedTab;
        
    }

    showChart()
    {
        // let doDraw = true;
        if(this.playerList.selected != -1){
            const player = this.playerList.list_playername[this.playerList.selected];
            const argumentIsObj = false;
            this.showChart_ofThisPlayer(player, argumentIsObj)                
            return;
        }
    
        try {
            let playerNameAsObj = {}; // this ugly trick seems needed for unknown reasons: https://stackoverflow.com/questions/74143407/javascript-convert-undefined-to-object-but-undefined-seems-a-string
            const argumentIsObj = true;
            // playerNameAsObj = this.searchPlayerInput.caption; // works
            playerNameAsObj =  Engine.ConfigDB_GetValue("user", "localratings.save.searchplayerinput"); // works
            // const playerData = this.historyDatabase[playerNameAsObj]; 
            this.showChart_ofThisPlayer(playerNameAsObj, argumentIsObj)

            // const playerStr = this.searchPlayerInput.caption; // this not works if name loaded from the config
            // const playerData = this.historyDatabase[playerStr]; // this not works if name loaded from the config

    
            // if (this.selectedTab.name == "tabEvolutionChart")
            //     this.currentChart = new LocalRatingsEvolutionChart(playerNameAsObj, playerData);
            // else if (this.selectedTab.name == "tabCivilizationChart")
            //     this.currentChart = new LocalRatingsCivilizationChart(playerNameAsObj, playerData);
            // else 
            //     doDraw = false;
            // if(doDraw)
            // this.currentChart.draw();
        } catch (error2) {
            error('near line 288');
        }
    }

    showPlayerProfile()
    {
        if (this.playerList.selected == -1)
            return;

        const player = this.playerList.list_playername[this.playerList.selected];
        const rank = this.playerList.list_rank[this.playerList.selected];
        const playerData = this.historyDatabase[player];
        this.currentPlayerProfile = new LocalRatingsPlayerProfile(player, playerData, rank);
        this.currentPlayerProfile.fill();
    }

    getColumns()
    {
        // Filter players
        const filterObj = new LocalRatingsPlayerFilter(this.ratingsDatabase);
        const filteredRatings = Object.entries(this.ratingsDatabase).filter(([key, value]) => !filterObj.applies(key));
        const filteredRatingsDatabase = Object.fromEntries(filteredRatings);

        // Create an array of players sorted by rank
        let sortedByRank = Object.keys(filteredRatingsDatabase).map(x => [
            x,
            filteredRatingsDatabase[x].rating,
            filteredRatingsDatabase[x].matches
        ]);
        sortedByRank.sort((a, b) => b[1] - a[1]);

        // Return JSON rating database into column format
        return {
            "rank": sortedByRank.map((x, i) => i+1),
            "player": sortedByRank.map(x => PlayerColor.ColorPlayerName(x[0], "", "")),
            "rating": sortedByRank.map(x => formatRating_LocalRatings(x[1])),
            "matches": sortedByRank.map(x => x[2]),
            "playername": sortedByRank.map(x => x[0])
        };
    }

    sortBySelectedColumn()
    {

        let rows = this.columns.rank.map((x, i) => [
            x,
            this.columns.player[i],
            this.columns.rating[i],
            this.columns.matches[i],
            this.columns.playername[i]
        ]);
        let column = this.playerList.selected_column;
        if(this.isColumnSorted == false){
            let column = Engine.ConfigDB_GetValue("user", "localratings.save.selectedcolumn");
            this.playerList.selected_column_order = ( Engine.ConfigDB_GetValue("user", "localratings.save.selectedcolumnorder")) == "-1" ? -1 : 1 ;
            // error('382: column, this.playerList.selected_column_order: ' + column + ", " + this.playerList.selected_column_order );
        }else{
            Engine.ConfigDB_WriteValueToFile("user", "localratings.save.selectedcolumn", column, "config/user.cfg");
            Engine.ConfigDB_WriteValueToFile("user", "localratings.save.selectedcolumnorder", this.playerList.selected_column_order, "config/user.cfg");
        }

        if(this.isColumnSorted == false)
            this.isColumnSorted = true;

        // error('378: column, column_order: ' + column + ", " + this.playerList.selected_column_order );

        // if(!this.playerList.selected_column){
        //     error('359: selected_column is empty');
        //     return
        // }
        // if(this.playerList.selected_column == ""){
        //     error('363: selected_column is empty');
        //     return
        // }



        if (column == "rank" || column == "rating")
            rows.sort((a, b) => b[0]-a[0]);
        else if (column == "matches")
            rows.sort((a, b) => (b[3] == a[3]) ? b[0]-a[0] : b[3]-a[3]);
        else
            rows.sort((a, b) => b[4].localeCompare(a[4]));
        if (this.playerList.selected_column_order < 0)
            rows.reverse();
        this.columns = {
            "rank": rows.map(x => x[0]),
            "player": rows.map(x => x[1]),
            "rating": rows.map(x => x[2]),
            "matches": rows.map(x => x[3]),
            "playername": rows.map(x => x[4])
        };
    }

    filterBySearchPlayerInput()
    {
        const text = this.searchPlayerInput.caption.toLowerCase();
        const filterPlayer = (pos, text) => this.columns.player[pos].toLowerCase().includes(text);
        return {
            "rank": this.columns.rank.filter((x, i) => filterPlayer(i, text)),
            "player": this.columns.player.filter((x, i) => filterPlayer(i, text)),
            "rating": this.columns.rating.filter((x, i) => filterPlayer(i, text)),
            "matches": this.columns.matches.filter((x, i) => filterPlayer(i, text)),
            "playername": this.columns.playername.filter((x, i) => filterPlayer(i, text))
        };
    }

    populate(columns)
    {
        this.playerList.selected = -1;
        this.playerList.list_rank = columns.rank;
        this.playerList.list_player = columns.player;
        this.playerList.list_rating = columns.rating;
        this.playerList.list_matches = columns.matches;
        this.playerList.list_playername = columns.playername;
        // Change these last, otherwise crash
        this.playerList.list = columns.player;
        this.playerList.list_data = columns.player;
    }

    updateList()
    {
        // If no columns, return
        this.columns = this.getColumns();
        if (Object.keys(this.columns).length == 0)
            return;

        // Populate table with columns
        this.sortBySelectedColumn();
        const columns = this.filterBySearchPlayerInput();
        this.populate(columns);



        const playerFirstInList = this.playerList.list_playername[0];
        // error("updateList = " + playerFirstInList)
        if(playerFirstInList){
            const player = this.playerList.list_playername[playerFirstInList];
            // error("updateList: " + playerFirstInList)
            // this.showChart();
            const argumentIsObj = false;
            this.showChart_ofThisPlayer(playerFirstInList, argumentIsObj);

        }

    }

}
