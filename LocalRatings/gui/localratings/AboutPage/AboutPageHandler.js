class LocalRatingsAboutPage
{

    constructor()
    {
        // GUI objects
        this.aboutText = Engine.GetGUIObjectByName("aboutText");
        this.closeButton = Engine.GetGUIObjectByName("closeButton");
        this.websiteButton = Engine.GetGUIObjectByName("websiteButton");
        this.forumButton = Engine.GetGUIObjectByName("forumButton");

        // Events
        this.closeButton.onPress = this.onPressCloseButton.bind(this);
        this.websiteButton.onPress = this.onPressWebsiteButton.bind(this);
        this.forumButton.onPress = this.onPressForumButton.bind(this);

        this.init();
    }

    onPressWebsiteButton()
    {
        openURL("https://gitlab.com/mentula0ad/LocalRatings");
    }

    onPressForumButton()
    {
        openURL("https://wildfiregames.com/forum/topic/80151-localratings-mod-evaluate-players-skills-based-on-previous-games/#comment-497805");
    }

    onPressCloseButton()
    {
        Engine.PopGuiPage();
    }

    parseContent(list)
    {
        let text = "";
        for (let obj of list)
        {
            text += setStringTags(obj.title + "\n\n", { "font": "sans-bold-20" });
            text += obj.text.reduce((pv, cv) => pv + "\n" + cv) + "\n\n\n";
        }

        return text;
    }

    init()
    {
        const json = Engine.ReadJSONFile("gui/localratings/AboutPage/about.json");
        let panelData = Object.keys(json).map(x => ({"label": x, "content": this.parseContent(json[x])}));
        placeTabButtons(
	    panelData,
	    false,
	    35,
	    5,
	    selectPanel,
	    category => { this.aboutText.caption = panelData[category].content; }
        );
    }

}
