/**
 * This class is responsible for performing the rating computation.
 * It operates only onto its attributes and not on actual database files stored in the cache.
 */
class LocalRatingsCalculator
{

    constructor()
    {
        this.ratingsDatabase = {};
        this.historyDatabase = {};
    }

    getScore(replayObj, player)
    {
        const playerIndex = replayObj.players.indexOf(player);
        const scores = replayObj.scores[playerIndex];

        // Compute score
        const weights = new LocalRatingsWeights();
        let score = 0;
        for (let scoreKey in scores)
        {
            let configKey = scoreKey.toLowerCase();
            let weight = weights[configKey];
            if (weight > 0)
                score += scores[scoreKey] * weight;
        }

        return score;
    }

    getRating(playerScore, averageScore)
    {
        return (averageScore == 0) ? 0 : (playerScore - averageScore) / averageScore;
    }

    run(replayDatabase)
    {
        // Load match filters
        const filterObj = new LocalRatingsFilter();

        // Generate a temporary history database
        let tmpHistoryDatabase = {};
        for (let replayObj of Object.values(replayDatabase))
        {
            // Skip replay if invalid
            if (!replayObj.isValid)
                continue;

            // Skip replay if filters apply
            if (filterObj.applies(replayObj))
                continue;

            // Compute replay data
            const players = replayObj.players;
            const scores = players.map(x => this.getScore(replayObj, x));
            const averageScore = getMean_LocalRatings(scores);
            const ratings = players.map((x, i) => this.getRating(scores[i], averageScore));

            // Update temporary history database
            for (let player in players)
            {
                let playerName = players[player];
                if (!(playerName in tmpHistoryDatabase))
                    tmpHistoryDatabase[playerName] = {};
                tmpHistoryDatabase[playerName][replayObj.directory] = {
                    "rating": ratings[player],
                    "civ": replayObj.civs[player]
                }
            }
        }

        // Now that temporary history database is populated, we can update the databases
        for (let player in tmpHistoryDatabase)
        {
            const tmpMatches = Object.values(tmpHistoryDatabase[player]).length;
            const tmpRating = getMean_LocalRatings(Object.values(tmpHistoryDatabase[player]).map(x => x.rating))

            // Known player
            if (player in this.ratingsDatabase)
            {
                const oldMatches = this.ratingsDatabase[player].matches;
                const oldRating = this.ratingsDatabase[player].rating;
                const newMatches = oldMatches + tmpMatches;
                const newRating = (oldRating*oldMatches + tmpRating*tmpMatches) / newMatches;
                this.ratingsDatabase[player] = {
                    "rating": newRating,
                    "matches": newMatches
                };
                Object.assign(this.historyDatabase[player], tmpHistoryDatabase[player]);
            }

            // Unknown player
            else
            {
                this.ratingsDatabase[player] = {
                    "rating": tmpRating,
                    "matches": tmpMatches
                };
                this.historyDatabase[player] = Object.assign({}, tmpHistoryDatabase[player]);
            }
        }
    }

    rebuild()
    {
        this.ratingsDatabase = {};
        this.historyDatabase = {};
        const replayDB = new LocalRatingsReplayDB();
        replayDB.load();
        this.run(replayDB.replayDatabase);
    }

    merge(newReplays)
    {
        this.run(newReplays);
    }

}
