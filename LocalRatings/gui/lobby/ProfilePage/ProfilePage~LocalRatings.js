/**
 * This class helps replacing the ProfilePage class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * The onProfile() method is responsible for displaying a player's profile, hence we want to modify it so that the rating is displayed next to each player's name.
 */
class ProfilePage_LocalRatings extends ProfilePage
{

    constructor(xmppMessages)
    {
        super(xmppMessages);
    }

    onProfile()
    {
        super.onProfile();

        // If option is not active, return
        const show = Engine.ConfigDB_GetValue("user", "localratings.general.showplayerprofile");
        if (show !== "true")
            return;

        // This is already run inside super.onProfile() but needs to be run again
	let attributes = Engine.GetProfile()[0];
	if (this.profilePage.hidden || this.requestedPlayer != attributes.player)
	    return;

	let profileFound = attributes.rating != "-2";
        if (!profileFound)
            return;

        if (this.requestedPlayer in g_LocalRatingsDatabase)
        {
            const playerData = g_LocalRatingsDatabase[this.requestedPlayer];
            this.profilePlayernameText.caption = addRating_LocalRatings(this.profilePlayernameText.caption, playerData);
        }
    }

}

ProfilePage = ProfilePage_LocalRatings;
