/**
 * This class is responsible for reading the user-defined player filters from the user.cfg configuraton file and use them to check whether a replay object must be filtered or not.
 * The method applies(playerName) returns true if player must be filtered, false otherwise.
 */
class LocalRatingsPlayerFilter
{

    constructor(ratingsDatabase)
    {
        this.ratingsDatabase = ratingsDatabase;
    }

    filterMinGames(playerName)
    {
        const minGamesValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.mingames");
        const minGamesValueInt = parseInt(minGamesValue);
        return (this.ratingsDatabase[playerName].matches < minGamesValueInt);
    }

    filterMaxGames(playerName)
    {
        const limitMaxGamesValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.limitmaxgames");
        const limitMaxGamesValueBoolean = (limitMaxGamesValue === "true");
        if (!limitMaxGamesValueBoolean)
            return false;
        const maxGamesValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.maxgames");
        const maxGamesValueInt = parseInt(maxGamesValue);
        return (this.ratingsDatabase[playerName].matches > maxGamesValueInt);
    }

    filterMinRating(playerName)
    {
        const limitMinRatingValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.limitminrating");
        const limitMinRatingValueBoolean = (limitMinRatingValue === "true");
        if (!limitMinRatingValueBoolean)
            return false;
        const minRatingValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.minrating");
        const minRatingValueFloat = parseFloat(minRatingValue);
        return (this.ratingsDatabase[playerName].rating < minRatingValueFloat/100);
    }

    filterMaxRating(playerName)
    {
        const limitMaxRatingValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.limitmaxrating");
        const limitMaxRatingValueBoolean = (limitMaxRatingValue === "true");
        if (!limitMaxRatingValueBoolean)
            return false;
        const maxRatingValue = Engine.ConfigDB_GetValue("user", "localratings.playerfilter.maxrating");
        const maxRatingValueFloat = parseFloat(maxRatingValue);
        return (this.ratingsDatabase[playerName].rating > maxRatingValueFloat/100);
    }

    // Main function

    applies(playerName)
    {
        if (this.filterMinGames(playerName))
            return true;
        if (this.filterMaxGames(playerName))
            return true;
        if (this.filterMinRating(playerName))
            return true;
        if (this.filterMaxRating(playerName))
            return true;
        return false;
    }

}
