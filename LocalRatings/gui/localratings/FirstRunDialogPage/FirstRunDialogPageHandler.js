class LocalRatingsFirstRunDialogPage
{

    constructor()
    {
        // GUI objects
        this.updateLaterButton = Engine.GetGUIObjectByName("updateLaterButton");
        this.updateNowButton = Engine.GetGUIObjectByName("updateNowButton");

        // Events
        this.updateLaterButton.onPress = this.onPressUpdateLater.bind(this);
        this.updateNowButton.onPress = this.onPressUpdateNow.bind(this);
    }

    closePage(update)
    {
        Engine.PopGuiPage({"update": update});
    }

    onPressUpdateLater()
    {
        this.closePage(false);
    }

    onPressUpdateNow()
    {
        this.closePage(true);
    }

}
