// Store changes
var g_LocalRatingsChanges = {};
var g_LocalRatingsSavedChanges = {};
var g_LocalRatingsRatingsDatabase;
var g_LocalRatingsHistoryDatabase;

function init(data)
{
    g_ChangedKeys = new Set();
    g_TabCategorySelected = 0;
    g_OptionType.color.tooltip = ""; // Remove "Default:" from color tooltips.
    g_Options = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json"); // This line is changed
    translateObjectKeys(g_Options, ["label", "tooltip"]);
    deepfreeze(g_Options);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions);
}

// Wrapper for the "displayOptions" function
function displayOptions_Wrapper_LocalRatings(target, args)
{
    // Run aditional code. This lines are needed, otherwise reset does not work properly
    const settings = new LocalRatingsSettings();
    settings.createDefaultSettingsIfNotExist();
    // Run original function
    target(...args);
    // Run additional code
    const savedSettings = settings.getSaved();
    for (let i=0; i<g_Options[g_TabCategorySelected].options.length; ++i)
    {
        let option = g_Options[g_TabCategorySelected].options[i];
        let optionType = g_OptionType[option.type];
        let control = Engine.GetGUIObjectByName("option_control_" + option.type + "[" + i + "]");
        let guiSetter = control[optionType.guiSetter];
        let configKey = option.config;
        control[optionType.guiSetter] = function() {
            guiSetter();
            // Run additional code
	    let value = optionType.guiToValue(control);
            (value != savedSettings[configKey]) ? g_LocalRatingsChanges[configKey] = value : delete g_LocalRatingsChanges[configKey];
	};
    }
};

// Handler for the "displayOptions" function
const displayOptions_Handler_LocalRatings = {
    apply: function(target, thisArg, args) {
        return displayOptions_Wrapper_LocalRatings(target, args);
    }
};

// Proxy the "displayOptions" function
displayOptions = new Proxy(displayOptions, displayOptions_Handler_LocalRatings);

// Wrapper for the "revertChanges" function
function revertChanges_Wrapper_LocalRatings(target, args)
{
    // Run original function
    target(...args);
    // Run additional code
    g_LocalRatingsChanges = {};
};

// Handler for the "revertChanges" function
const revertChanges_Handler_LocalRatings = {
    apply: function(target, thisArg, args) {
        return revertChanges_Wrapper_LocalRatings(target, args);
    }
};

// Proxy the "revertChanges" function
revertChanges = new Proxy(revertChanges, revertChanges_Handler_LocalRatings);

// Wrapper for the "reallySaveChanges" function
function reallySaveChanges_Wrapper_LocalRatings(target, args)
{
    // Run original function
    target(...args);
    // Run additional code
    updateGlobalsIfRequired_LocalRatings();
    g_LocalRatingsSavedChanges = Object.assign({}, g_LocalRatingsChanges);
};

// Handler for the "reallySaveChanges" function
const reallySaveChanges_Handler_LocalRatings = {
    apply: function(target, thisArg, args) {
        return reallySaveChanges_Wrapper_LocalRatings(target, args);
    }
};

// Proxy the "reallySaveChanges" function
reallySaveChanges = new Proxy(reallySaveChanges, reallySaveChanges_Handler_LocalRatings);

// Wrapper for the "reallySetDefaults" function
function reallySetDefaults_Wrapper_LocalRatings(target, args)
{
    // We save previous settings, to check changes occurred
    const settings = new LocalRatingsSettings();
    const savedSettings = settings.getSaved();

    // Run original function
    target(...args);

    // Run additional code
    g_LocalRatingsChanges = {};
    const defaultSettings = settings.getDefault();
    for (let key in defaultSettings)
    {
        let value = defaultSettings[key];
        if (value != savedSettings[key])
            g_LocalRatingsChanges[key] = value;
    }

    // Update global variables
    updateGlobalsIfRequired_LocalRatings();
    g_LocalRatingsSavedChanges = Object.assign({}, g_LocalRatingsChanges);
};

// Handler for the "reallySetDefaults" function
const reallySetDefaults_Handler_LocalRatings = {
    apply: function(target, thisArg, args) {
        return reallySetDefaults_Wrapper_LocalRatings(target, args);
    }
};

// Proxy the "reallySetDefaults" function
reallySetDefaults = new Proxy(reallySetDefaults, reallySetDefaults_Handler_LocalRatings);

// Overwrite the "closePageWithoutConfirmation" function
function closePageWithoutConfirmation()
{
    // Revert changes, so that changes are not stored for the session;
    revertChanges();
    // Quit
    Engine.PopGuiPage({
        "ratingsDatabase": g_LocalRatingsRatingsDatabase,
        "historyDatabase": g_LocalRatingsHistoryDatabase,
        "changes": g_LocalRatingsSavedChanges
    });
}

// Check if changes are made. If so, populate the corresponding global variables
function updateGlobalsIfRequired_LocalRatings()
{
    const keysToUpdate = ["localratings.filter.", "localratings.weight."];
    if (Object.keys(g_LocalRatingsChanges).some(x => keysToUpdate.some(y => x.startsWith(y))))
    {
        // Update ratings and history databases
        let ratingsDB = new LocalRatingsRatingsDB();
        ratingsDB.rebuild();

        // Update globals
        g_LocalRatingsRatingsDatabase = ratingsDB.ratingsDatabase;
        g_LocalRatingsHistoryDatabase = ratingsDB.historyDatabase;
    }
}
