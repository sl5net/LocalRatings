/**
 * This class is responsible for dealing with settings.
 */
class LocalRatingsSettings
{

    getSaved()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json")
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = Engine.ConfigDB_GetValue("user", option.config)));
        return settings;
    }

    getDefault()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json")
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = option.val.toString()));
        return settings;
    }

    createDefaultSettingsIfNotExist()
    {
        const settings = this.getDefault();
        for (const key in settings)
        {
            if (! Engine.ConfigDB_GetValue("user", key))
            {
                Engine.ConfigDB_CreateValue("user", key, settings[key]);
                Engine.ConfigDB_WriteFile("user", "config/user.cfg");
            }
        }
    }

}
