/**
 * This class is responsible for updating or rebuilding the replay database stored in the cache folder.
 */
class LocalRatingsReplayDB
{

    constructor()
    {
        this.replayDatabase = {};
        this.newReplays = {};
    }

    addReplays(replaySet, database)
    {
        for (let replay of replaySet)
        {
            let replayObj = new LocalRatingsReplay(replay);
            if (replayObj.isValid)
                database[replay.directory] = replayObj;
        }
    }

    empty()
    {
        return Object.keys(this.replayDatabase) == 0;
    }

    load()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();
        this.replayDatabase = minifier.magnifyReplayDatabase(cache.load("replayDatabase"));
    }

    rebuild()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();

        // If cache update is required, update version
        if (cache.isUpdateRequired())
            cache.updateVersion();

        // Generate replay database from scratch
        this.addReplays(Engine.GetReplays(), this.replayDatabase);

        // Save
        cache.save("replayDatabase", minifier.minifyReplayDatabase(this.replayDatabase));
    }

    update()
    {
        const cache = new LocalRatingsCache();
        const minifier = new LocalRatingsMinifier();

        // If cache update is required, full rebuild is needed
        if (cache.isUpdateRequired())
        {
            this.rebuild();
            return;
        }

        // Load
        this.replayDatabase = minifier.magnifyReplayDatabase(cache.load("replayDatabase"));

        // Update the replay database with new replays
        const unScanned = Engine.GetReplays().filter(x => !(x.directory in this.replayDatabase));
        if (unScanned.length > 0)
        {
            // Add new replays
            this.addReplays(unScanned, this.newReplays);
            Object.assign(this.replayDatabase, this.newReplays);
            // Save
            cache.save("replayDatabase", minifier.minifyReplayDatabase(this.replayDatabase));
        }
    }

}
