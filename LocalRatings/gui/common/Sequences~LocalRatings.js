/**
 * This class stores the information on the replay sequences.
 * Each attributes of this class corresponds (as a general principle) to a possible score weight that a user might want to use for the rating calculation.
 */
class LocalRatingsSequences
{

    constructor(sequences)
    {
        try
        {
            // Resources
            this.resourcesGathered = this.getResourcesGathered(sequences);
            this.resourcesUsed = this.getResourcesUsed(sequences);
            this.resourcesBought = this.getResourcesBought(sequences);
            this.resourcesSold = this.getResourcesSold(sequences);
            // Tributes
            this.tributesSent = this.getTributesSent(sequences);
            // Trade
            this.tradeIncome = this.getTradeIncome(sequences);
            // Units
            this.enemyUnitsKilledValue = this.getEnemyUnitsKilledValue(sequences);
            this.enemyUnitsKilled = this.getEnemyUnitsKilled(sequences);
            this.unitsCapturedValue = this.getUnitsCapturedValue(sequences);
            this.unitsCaptured = this.getUnitsCaptured(sequences);
            // Buildings
            this.enemyBuildingsDestroyedValue = this.getEnemyBuildingsDestroyedValue(sequences);
            this.enemyBuildingsDestroyed = this.getEnemyBuildingsDestroyed(sequences);
            this.buildingsCapturedValue = this.getBuildingsCapturedValue(sequences);
            this.buildingsCaptured = this.getBuildingsCaptured(sequences);
            // Map
            this.percentMapExplored = this.getPercentMapExplored(sequences);
            this.percentMapControlled = this.getPercentMapControlled(sequences);
            this.peakPercentMapControlled = this.getPeakPercentMapControlled(sequences);
            // Bribes
            this.successfulBribes = this.getSuccessfulBribes(sequences);

            // Check if all keys are defined, otherwise return empty object
            if (!this.validate())
                return {};
        }
        catch(error)
        {
            return {};
        }
    }

    sumArraysComponentWise(...arrays)
    {
        return arrays.reduce((array1, array2) => array1.map((a, i) => a + array2[i]));
    }

    validate()
    {
        // Validate top-level keys
        if (Object.values(this).some(x => !Array.isArray(x)))
            return false
        if (Object.values(this).some(x => x.length == 0))
            return false
        return true;
    }

    // RESOURCES

    getResourcesGathered(sequences)
    {
        return this.sumArraysComponentWise(
            sequences.resourcesGathered.food,
            sequences.resourcesGathered.wood,
            sequences.resourcesGathered.stone,
            sequences.resourcesGathered.metal
        );
    }

    getResourcesUsed(sequences)
    {
        return this.sumArraysComponentWise(
            sequences.resourcesUsed.food,
            sequences.resourcesUsed.wood,
            sequences.resourcesUsed.stone,
            sequences.resourcesUsed.metal
        );
    }

    getResourcesBought(sequences)
    {
        return this.sumArraysComponentWise(
            sequences.resourcesBought.food,
            sequences.resourcesBought.wood,
            sequences.resourcesBought.stone,
            sequences.resourcesBought.metal
        );
    }

    getResourcesSold(sequences)
    {
        return this.sumArraysComponentWise(
            sequences.resourcesSold.food,
            sequences.resourcesSold.wood,
            sequences.resourcesSold.stone,
            sequences.resourcesSold.metal
        );
    }

    // TRIBUTES

    getTributesSent(sequences)
    {
        return sequences.tributesSent;
    }

    getTributesReceived(sequences)
    {
        return sequences.tributesReceived;
    }

    // TRADE

    getTradeIncome(sequences)
    {
        return sequences.tradeIncome;
    }

    // UNITS

    getEnemyUnitsKilledValue(sequences)
    {
        return sequences.enemyUnitsKilledValue;
    }

    getEnemyUnitsKilled(sequences)
    {
        return sequences.enemyUnitsKilled.total;
    }

    getUnitsCapturedValue(sequences)
    {
        return sequences.unitsCapturedValue;
    }

    getUnitsCaptured(sequences)
    {
        return sequences.unitsCaptured.total;
    }

    // BUILDINGS

    getEnemyBuildingsDestroyedValue(sequences)
    {
        return sequences.enemyBuildingsDestroyedValue;
    }

    getEnemyBuildingsDestroyed(sequences)
    {
        return sequences.enemyBuildingsDestroyed.total;
    }

    getBuildingsCapturedValue(sequences)
    {
        return sequences.buildingsCapturedValue;
    }

    getBuildingsCaptured(sequences)
    {
        return sequences.buildingsCaptured.total;
    }

    // MAP

    getPercentMapExplored(sequences)
    {
        return sequences.percentMapExplored;
    }

    getPercentMapControlled(sequences)
    {
        return sequences.percentMapControlled;
    }

    getPeakPercentMapControlled(sequences)
    {
        return sequences.peakPercentMapControlled;
    }

    // BRIBES

    getSuccessfulBribes(sequences)
    {
        return sequences.successfulBribes;
    }

}
