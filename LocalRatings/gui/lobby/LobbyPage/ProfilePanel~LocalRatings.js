/**
 * This class helps replacing the ProfilePanel class.
 * The purpose is to overwrite its methods, providing extra functionalities.
 * The requestProfile() and onProfile() methods are responsible for displaying player profiles, hence we want to modify them so that the rating is displayed next to each player's name.
 */
class ProfilePanel_LocalRatings extends ProfilePanel
{

    constructor(xmppMessages, playerList, leaderboardPage)
    {
        super(xmppMessages, playerList, leaderboardPage);
    }

    requestProfile(playerName)
    {
        super.requestProfile(playerName);

        // If option is not active, return
        const show = Engine.ConfigDB_GetValue("user", "localratings.general.showplayerprofile");
        if (show !== "true")
            return;

        // Add rating only if player is not new to the database
        if (playerName in g_LocalRatingsDatabase)
        {
            const playerData = g_LocalRatingsDatabase[playerName];
            this.playernameText.caption = addRating_LocalRatings(this.playernameText.caption, playerData);
        }
    }

    onProfile()
    {
        super.onProfile();

        // If option is not active, return
        const show = Engine.ConfigDB_GetValue("user", "localratings.general.showplayerprofile");
        if (show !== "true")
            return;

        // This is already run inside super.onProfile() but needs to be run again
        let attributes = Engine.GetProfile()[0];
	if (attributes.rating == "-2" || attributes.player != this.requestedPlayer)
	    return;

        // Add rating only if player is not new to the database
        if (this.requestedPlayer in g_LocalRatingsDatabase)
        {
            const playerData = g_LocalRatingsDatabase[this.requestedPlayer];
            this.playernameText.caption = addRating_LocalRatings(this.playernameText.caption, playerData);
        }
    }

}

ProfilePanel = ProfilePanel_LocalRatings;
