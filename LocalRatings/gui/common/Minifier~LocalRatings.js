/**
 * This class helps in reducing the space occupied by a replay object in the database
 */
class LocalRatingsMinifier
{

    constructor()
    {
        // These keys MUST be the same as in Replay.js
        this.replayKeys = [
            "duration",
            "directory",
            "date",
            "players",
            "civs",
            "teamsSize",
            "startingResources",
            "populationCap",
            "worldPopulation",
            "hasAiPlayers",
            "cheatsEnabled",
            "revealedMap",
            "exploredMap",
            "nomad",
            "isValid",
            "scores"
        ];

        // These keys MUST be the same as in Sequences.js
        this.scoreKeys = [
            "resourcesGathered",
            "resourcesUsed",
            "resourcesBought",
            "resourcesSold",
            "tributesSent",
            "tradeIncome",
            "enemyUnitsKilledValue",
            "enemyUnitsKilled",
            "unitsCapturedValue",
            "unitsCaptured",
            "enemyBuildingsDestroyedValue",
            "enemyBuildingsDestroyed",
            "buildingsCapturedValue",
            "buildingsCaptured",
            "percentMapExplored",
            "percentMapControlled",
            "peakPercentMapControlled",
            "successfulBribes"
        ];

    }

    minifyNumber(number)
    {
        return Math.round(number * 1e4) / 1e4;
    }

    minifyReplay(replayObj)
    {
        return this.replayKeys.map(replayKey => {
            if (replayKey == "scores")
            {
                return replayObj[replayKey].map((playerScores, playerIndex) =>
                    this.scoreKeys.map(scoreKey =>
                        this.minifyNumber(replayObj[replayKey][playerIndex][scoreKey])
                    )
                );
            }
            else
                return replayObj[replayKey];
        });
    }

    magnifyReplay(minifiedReplayObj)
    {
        return Object.fromEntries(this.replayKeys.map((replayKey, replayKeyIndex) => {
            if (replayKey == "scores")
            {
                return [replayKey, minifiedReplayObj[replayKeyIndex].map((playerScores, playerIndex) =>
                    Object.fromEntries(this.scoreKeys.map((scoreKey, scoreKeyIndex) =>
                        [scoreKey, minifiedReplayObj[replayKeyIndex][playerIndex][scoreKeyIndex]]
                    ))
                )];
            }
            else
                return [replayKey, minifiedReplayObj[replayKeyIndex]];
        }));
    }

    minifyReplayDatabase(replayDatabase)
    {
        let minifiedReplayDatabase = {};
        for (let key in replayDatabase)
            minifiedReplayDatabase[key] = this.minifyReplay(replayDatabase[key]);
        return minifiedReplayDatabase;
    }

    magnifyReplayDatabase(minifiedReplayDatabase)
    {
        let replayDatabase = {};
        for (let key in minifiedReplayDatabase)
            replayDatabase[key] = this.magnifyReplay(minifiedReplayDatabase[key]);
        return replayDatabase;
    }

    minifyRatingsDatabase(ratingsDatabase)
    {
        let minifiedRatingsDatabase = {};
        for (let player in ratingsDatabase)
        {
            let playerData = ratingsDatabase[player];
            minifiedRatingsDatabase[player] = [this.minifyNumber(playerData.rating), playerData.matches];
        }
        return minifiedRatingsDatabase;
    }

    magnifyRatingsDatabase(minifiedRatingsDatabase)
    {
        let ratingsDatabase = {};
        for (let player in minifiedRatingsDatabase)
        {
            let playerData = minifiedRatingsDatabase[player];
            ratingsDatabase[player] = {"rating": playerData[0], "matches": playerData[1]};
        }
        return ratingsDatabase;
    }

    minifyHistoryDatabase(historyDatabase)
    {
        let minifiedHistoryDatabase = {};
        for (let player in historyDatabase)
        {
            let playerData = {};
            for (let replayDir in historyDatabase[player])
            {
                let replayData = historyDatabase[player][replayDir];
                playerData[replayDir] = [this.minifyNumber(replayData.rating), replayData.civ];
            }
            minifiedHistoryDatabase[player] = playerData;
        }
        return minifiedHistoryDatabase;
    }

    magnifyHistoryDatabase(minifiedHistoryDatabase)
    {
        let historyDatabase = {};
        for (let player in minifiedHistoryDatabase)
        {
            let playerData = {};
            for (let replayDir in minifiedHistoryDatabase[player])
            {
                let replayData = minifiedHistoryDatabase[player][replayDir];
                playerData[replayDir] = {"rating": replayData[0], "civ": replayData[1]};
            }
            historyDatabase[player] = playerData;
        }
        return historyDatabase;
    }

}
