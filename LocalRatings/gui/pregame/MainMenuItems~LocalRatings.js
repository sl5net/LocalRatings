// Add the Local Ratings menu button to the 0 A.D. main menu
g_MainMenuItems.splice(g_MainMenuItems.length - 1, 0,
    {
	"caption": translate("Local Ratings"),
	"tooltip": translate("Rank 0 A.D. players based on games played with them."),
	"onPress": () => {
	    Engine.SwitchGuiPage("page_localratings.xml");
	}
    }
);
