class LocalRatingsEmptyListDialogPage
{

    constructor()
    {
        // GUI objects
        this.closeButton = Engine.GetGUIObjectByName("closeButton");

        // Events
        this.closeButton.onPress = this.onPressClose.bind(this);
    }

    onPressClose()
    {
        Engine.PopGuiPage();
    }

}
