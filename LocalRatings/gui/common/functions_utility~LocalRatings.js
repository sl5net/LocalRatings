/**
 * Convert a number to a string with two decimal digits
 * @param {number} value
 * @returns {string}
 */
function formatRating_LocalRatings(value)
{
    return (value * 100).toFixed(2);
}

/**
 * Return the mean of a non-empty numeric array
 * @param {array} array
 * @returns {number}
 */
function getMean_LocalRatings(array)
{
    return array.reduce((pv, cv) => pv + cv, 0) / array.length;
}

/**
 * Return the name of a player, with the addition of the rating and the number of matches in gray color
 * @param {string} text
 * @param {object} playerData
 * @param {number} playerData.rating
 * @param {number} playerData.matches
 * @returns {string}
 */
function addRating_LocalRatings(text, playerData)
{
    return text  + coloredText(" (" + formatRating_LocalRatings(playerData.rating) + "/" + playerData.matches + ")", "gray");
}

/**
 * Return the ratings database, after performing an update with new replays
 * @returns {object}
 */
function init_LocalRatings()
{
    // Write default settings in user.cfg if not present
    const settings = new LocalRatingsSettings();
    settings.createDefaultSettingsIfNotExist();

    // If cache version has changed, we empty all databases and we do nothing.
    // Only a full rebuild will re-populate databases
    const cache = new LocalRatingsCache();
    if (cache.isUpdateRequired())
    {
        cache.save("replayDatabase", {});
        cache.save("ratingsDatabase", {});
        cache.save("historyDatabase", {});

        return {};
    }

    // Update the replay database with new replays and save ratings and history databases
    let replayDB = new LocalRatingsReplayDB();
    let ratingsDB = new LocalRatingsRatingsDB();

    replayDB.update();
    ratingsDB.load();
    ratingsDB.merge(replayDB.newReplays);
    ratingsDB.save();

    return ratingsDB.ratingsDatabase;
}
