class LocalRatingsPlayerProfile
{

    constructor(player, playerData, rank)
    {
        // GUI object
        this.playerProfile = Engine.GetGUIObjectByName("playerProfile");
        this.playerNameText = Engine.GetGUIObjectByName("playerNameText");
        this.currentRankText = Engine.GetGUIObjectByName("currentRankText");
        this.currentRatingText = Engine.GetGUIObjectByName("currentRatingText");
        this.highestRatingText = Engine.GetGUIObjectByName("highestRatingText");
        this.lowestRatingText = Engine.GetGUIObjectByName("lowestRatingText");
        this.bestPerformanceText = Engine.GetGUIObjectByName("bestPerformanceText");
        this.worstPerformanceText = Engine.GetGUIObjectByName("worstPerformanceText");
        this.totalMatchesText = Engine.GetGUIObjectByName("totalMatchesText");
        this.ratingStandardDeviationText = Engine.GetGUIObjectByName("ratingStandardDeviationText");
        this.performanceStandardDeviationText = Engine.GetGUIObjectByName("performanceStandardDeviationText");

        this.player = player;
        this.playerData = playerData;
        this.rank = rank;
    }

    getSingleGamesRatings()
    {
        return Object.keys(this.playerData).sort().map(x => this.playerData[x].rating);
    }

    getAverageRatings(singleGamesRatings)
    {
        let singleGamesRatingsSum = [singleGamesRatings[0]];
        for (let i=1; i<singleGamesRatings.length; i++)
            singleGamesRatingsSum.push(singleGamesRatingsSum[i-1] + singleGamesRatings[i]);
        return singleGamesRatingsSum.map((x, i) => x / (i+1));
    }

    getStandardDeviation(singleGamesRatings, mean)
    {
        return Math.sqrt(singleGamesRatings.reduce((pv, cv) => pv + (cv-mean)**2, 0) / singleGamesRatings.length);
    }

    fill()
    {
        const singleGamesRatings = this.getSingleGamesRatings();
        const averageRatings = this.getAverageRatings(singleGamesRatings);
        const currentRating = averageRatings[averageRatings.length-1];
        const ratingStandardDeviation = this.getStandardDeviation(averageRatings, currentRating);
        const performanceStandardDeviation = this.getStandardDeviation(singleGamesRatings, currentRating);

        this.playerNameText.caption = PlayerColor.ColorPlayerName(this.player, "", "");
        this.currentRankText.caption = this.rank;
        this.currentRatingText.caption = formatRating_LocalRatings(currentRating);
        this.highestRatingText.caption = formatRating_LocalRatings(Math.max(...averageRatings));
        this.lowestRatingText.caption = formatRating_LocalRatings(Math.min(...averageRatings));
        this.bestPerformanceText.caption = formatRating_LocalRatings(Math.max(...singleGamesRatings));
        this.worstPerformanceText.caption = formatRating_LocalRatings(Math.min(...singleGamesRatings));
        this.totalMatchesText.caption = singleGamesRatings.length;
        this.ratingStandardDeviationText.caption = formatRating_LocalRatings(ratingStandardDeviation);
        this.performanceStandardDeviationText.caption = formatRating_LocalRatings(performanceStandardDeviation);
        
        this.playerProfile.hidden = false;
    }

    clear()
    {
        this.playerProfile.hidden = true;
    }
    
}
