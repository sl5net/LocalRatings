/**
 * This class is responsible for reading and storing the user-defined player weights from the user.cfg configuraton file.
 */
class LocalRatingsWeights
{

    constructor()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/localratings/OptionsPage/options.json");
        const configKeys = optionsJSON.filter(x => x.label === "Score Weights")[0].options.map(x => x.config);
        const configKeyType = "localratings.weight.";
        const configKeyTypeLength = configKeyType.length;
        for (let fullConfigKey of configKeys)
        {
            let configKey = fullConfigKey.substring(configKeyTypeLength);
            this[configKey] = parseFloat(Engine.ConfigDB_GetValue("user", fullConfigKey));
        }
    }

}
