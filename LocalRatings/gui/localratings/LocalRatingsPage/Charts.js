class LocalRatingsChart
{

    constructor(player, playerData)
    {
        this.chartContainer;
        this.player = player;
        this.playerData = playerData;
        this.configOptions = new LocalRatingsChartsOptions().configOptions;

        // Animation
        this.animationIntervals = 10;
        this.currentAnimation = 0;
    }

    onChartContainerTick(animatingFunction)
    {
        if (this.currentAnimation > this.animationIntervals)
        {
            delete this.chartContainer.onTick;
            return;
        }

        const step = this.currentAnimation / this.animationIntervals;
        this.animate(step);
        this.currentAnimation += 1;
    }

    animate(step)
    {
    }

    draw()
    {
    }

    clear()
    {
        this.chartContainer.hidden = true;
    }
    
}
